package be.nrb.steffen.sonarcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SonarCicdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SonarCicdApplication.class, args);
    }

}